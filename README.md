# Secured File Transfer application

Application uses pcap library to create packets on all OSI layers and transfer file securely over Ethernet and WiFi in parallel

## Getting started
### Windows
- Download & Install WinPcap ([Download Link](http://www.winpcap.org/install/default.htm))
- Download & Install CMake ([Download Link](https://cmake.org/download/))
- Run `GenerateProject.bat` to generate project files for Visual Studio 2013 or change file to generate project files from another IDE
- Open `build` directory and open project using your IDE

### Linux
- Install CMake & Pcap `sudo apt-get install libpcap-dev cmake`
- Run `GenerateProject.sh` or `cmake .`
- Build with `make`
- Run executable `bin/FileTransfer.*` with **sudo**!
