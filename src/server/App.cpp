#include <iostream>
#include <thread>
#include "pcap.h"
#include "Scheduler.h"

using namespace std;

int main() {
	Scheduler scheduler = Scheduler(SCHEDULER_TYPE_SENDER);

	this_thread::sleep_for(chrono::milliseconds(1000));
	scheduler.sendFile("transfer.jpg");

	return 0;
}
