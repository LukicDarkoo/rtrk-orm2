add_subdirectory(core)

#set(SERVER_SRC_FILES ${CORE_SRC_FILES})
#set(CLIENT_SRC_FILES ${CORE_SRC_FILES})

add_subdirectory(client)
add_subdirectory(server)

if (UNIX)
	# Linux
	add_library(FileTransfer.Core ${CORE_SRC_FILES})
	target_link_libraries(FileTransfer.Core pcap pthread)

	add_executable(FileTransfer.Server ${SERVER_SRC_FILES})
	target_link_libraries(FileTransfer.Server FileTransfer.Core pcap pthread)

	add_executable(FileTransfer.Client ${CLIENT_SRC_FILES})
	target_link_libraries(FileTransfer.Client FileTransfer.Core pcap pthread)
	
else ()
	# Windows
	add_library(FileTransfer.Core ${CORE_SRC_FILES})
	target_link_libraries(FileTransfer.Core wpcap Ws2_32)

	add_executable(FileTransfer.Server ${SERVER_SRC_FILES})
	target_link_libraries(FileTransfer.Server FileTransfer.Core wpcap)

	add_executable(FileTransfer.Client ${CLIENT_SRC_FILES})
	target_link_libraries(FileTransfer.Client FileTransfer.Core wpcap)
endif (UNIX)
