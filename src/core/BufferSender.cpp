#include "BufferSender.h"

string BufferSender::TAG = "BufferSender";

BufferSender::BufferSender(string filename):
file(filename, ios::in | ios::binary),
nextPacketIndex(0),
topPacketIndex(0),
windowIndex(0),
readFileFinished(false),
windowStartTopPacketIndex(0),
done(false) {

	// Check fileopen
	if (file.is_open()) {
		file.seekg(0, ios::beg);
	}

	else {
		Log::d(TAG, "ERROR: Cannot read file!");
		exit(0);
	}

	// Fill buffer
	fillBuffer();
}

BufferSender::~BufferSender() {
	Log::d(TAG, "~BufferSender");
	file.close();
}

Packet BufferSender::getPacket() {
	windowIndex++;
	return getPacket(topPacketIndex++);
}

void BufferSender::loadNextWindow() {
	windowIndex = 0;
	windowStartTopPacketIndex = topPacketIndex;

	buffer.clear();
	Log::d(TAG, buffer.size());

	fillBuffer();
}

bool BufferSender::isWindowOver() {
	return (windowIndex >= BUFFER_SIZE);
}

Packet BufferSender::getPacket(unsigned int packetIndex) {
	Packet packet;

	// Check is over
	if (nextPacketIndex == packetIndex && readFileFinished == true) {
		done = true;
		packet.setCode(CODE_FINISH);
		Log::d(TAG, "File transfer finish");
		
		// Don't allow this packet before other data packets
		this_thread::sleep_for(chrono::milliseconds(500));

		return packet;
	}

	// Get packet
	unique_lock<mutex> l(m);
	if (buffer.find(packetIndex) != buffer.end()) {
		packet = buffer[packetIndex];
	}
	else {
		Log::d(TAG, "Please extend `BUFFER_SIZE`, if still doesn't work idea is total failure");
		Log::d(TAG, packetIndex);
		packet.setCode(CODE_INVALID);
		return packet;
	}

	return packet;
}

void BufferSender::prepareWindowRetransmission() {
	windowIndex = 0;
	topPacketIndex = windowStartTopPacketIndex;
	Log::d(TAG, "Window retransmission");
}

void BufferSender::fillBuffer() {
	char block[PACKET_DATA_LENGTH];
	memset(block, 0, PACKET_DATA_LENGTH);

	// Read file
	while (file.good() == true && isFull() == false) {
		file.read(block, PACKET_DATA_LENGTH);
		Packet packet = Packet();
		packet.setIndex(nextPacketIndex);
		packet.setData(block);
		buffer[nextPacketIndex] = packet;
		nextPacketIndex++;
		Log::d(TAG, "packet added to buffer");
	}

	if (file.good() == false) {
		readFileFinished = true;
	}
}
