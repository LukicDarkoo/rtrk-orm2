#include "Transporter.h"

string Transporter::TAG = "Transporter";


Transporter::~Transporter() {
	threadPacketListener.join();
	Log::d(TAG, "Transporter::~Transporter");
}

Transporter::Transporter(pcap_t *_adhandle) {
	this->adhandle = _adhandle;
	threadPacketListener = thread(bind(&Transporter::packetListenerHandler, this));
	Log::d(TAG, "start listening");
}

int Transporter::sendPacket(Packet packet) {
	u_char data[TOTAL_PACKET_LENGTH];
	packet.extract(data);
	Log::d(TAG, "packet almost sent");
	return pcap_sendpacket(this->adhandle, data, TOTAL_PACKET_LENGTH);
}

void Transporter::packetListenerHandler() {
	static int index = 0;

	pcap_pkthdr *pkt_header;
	const u_char *pkt_data;

	while (true) {
		{
			unique_lock<mutex> l(mutextPacketListener);
			if (pcap_next_ex(adhandle, &pkt_header, &pkt_data) != 0) {
				try
				{
					Packet *packet = new Packet(pkt_data);

					if (packet->getPort() == PORT_NUMBER) {
						Log::d(TAG, "packet came");
						onPacketReceivedCallback(packet);
					}
				}
				catch (...) {}
			}
		}

		this_thread::sleep_for(chrono::microseconds(100 / RECIEVE_SPEED));
	}
}
