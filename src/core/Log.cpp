#include "Log.h"

void Log::d(string TAG, string msg) {
#if LOG_ENABLED == 1
	cout << TAG << ": " << msg << endl;
#endif
}

void Log::d(string TAG, int msg) {
#if LOG_ENABLED == 1
	cout << TAG << ": " << msg << endl;
#endif
}