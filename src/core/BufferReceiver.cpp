#include "BufferReceiver.h"

string BufferReceiver::TAG = "BufferReceiver";

BufferReceiver::BufferReceiver(string _filename):
	nextPacketIndex(0),
	packetIndexInsideWindow(0) {
	filename = _filename;

	buffer = map<int, Packet*>();

	file = new ofstream(_filename, ios::out | ios::binary);
}

bool BufferReceiver::isBlockFull() { 
	return BUFFER_SIZE <= packetIndexInsideWindow;
}


void BufferReceiver::clearBuffer() {
	for (const auto& kv : buffer) {
		Packet *packet = kv.second;
		if (packet != nullptr) {
			// CHECK: Throws access violation 0xFFFFFFF
			// delete packet;
		}
	}
	buffer.clear();
	packetIndexInsideWindow = 0;
}

BufferReceiver::~BufferReceiver() {
	Log::d(TAG, "~BufferReceiver");
	file->close();
	clearBuffer();
}

void BufferReceiver::finish() {
	tryToWrite();
	file->close();
}

void BufferReceiver::add(Packet *packet) {
	Log::d(TAG, "Packet came");
	// PROBLEM: Mutex m gets address 0x00
	unique_lock<mutex> l(m);
	if (packet->getIndex() >= nextPacketIndex) {
		buffer[packet->getIndex()] = packet;
		tryToWrite();
	}
	// ELSE: Packet came too late :(	
}

void BufferReceiver::tryToWrite() {
	while (buffer.find(nextPacketIndex) != buffer.end()) {
		char block[PACKET_DATA_LENGTH];
		buffer[nextPacketIndex]->getData(block);

		file->write(block, PACKET_DATA_LENGTH);

		if (file->fail()) {
			Log::d(TAG, "write error");
		}
		else {
			Log::d(TAG, "write succcess");
		}

		// Set next packet index
		nextPacketIndex++;

		packetIndexInsideWindow++;
	}
}