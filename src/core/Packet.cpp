#include "Packet.h"

/*
Params to change

ethernetHeader.DestinationMAC
ethernetHeader.SourceMAC
ipHeader.TotalLength
udpHeader.DatagramLength
*/

const string Packet::TAG = "Packet";

Packet::Packet() {
	// Set defaults for MAC Header
	ethernetHeader.DestinationMAC = MACAddress{ 0xff, 0xff, 0xff, 0xff, 0xff, 0xff };
	ethernetHeader.SourceMAC = MACAddress{ 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
	ethernetHeader.Type = ntohs(0x0800); // IPv4

	// Set defaults for IP Header
	ipHeader.VersionInternetHeaderLength = 0x45;
	ipHeader.TOS = 0x0;
	ipHeader.TotalLength = ntohs(TOTAL_PACKET_LENGTH - PACKET_ETHERNET_HEADER_LENGTH);
	ipHeader.Identification = 0x00;
	ipHeader.FlagsFragmetOffeset = 0;
	ipHeader.TimeToLive = 0x40; // 60
	ipHeader.Protocol = PROTOCOL_UDP;
	ipHeader.HeaderChecksum = 0x0;	// TODO: Make algorithm to calculate checksum
	ipHeader.SourceAddress = IPAddress{ 192, 168, 1, 136 };
	ipHeader.DestinationAddress = IPAddress{ 192, 168, 1, 244 };

	// Set UDP protocol as default
	// Set defaults for UDP Header
	udpHeader.SourcePort = PORT_NUMBER;
	udpHeader.DestinationPort = PORT_NUMBER;
	udpHeader.DatagramLength = ntohs(TOTAL_PACKET_LENGTH - PACKET_ETHERNET_HEADER_LENGTH - PACKET_IP_HEADER_LENGTH);
	udpHeader.Checksum = 0x0; // TODO: Make algorithm to calculate checksum
	udpHeader.Code = CODE_TRANSFER;
	udpHeader.PacketIndex = 0;

	// Empty data
	memset(data, 0, PACKET_DATA_LENGTH);
}

int Packet::setData(char *_data) {
	for (int i = 0; i < PACKET_DATA_LENGTH; i++) {
		data[i] = _data[i];
	}

	return T_SUCCESS;
}

int Packet::getData(char *_data) {
	for (int i = 0; i < PACKET_DATA_LENGTH; i++) {
		_data[i] = data[i];
	}

	return T_SUCCESS;
}

unsigned short Packet::getPort() {
	return udpHeader.DestinationPort;
}

int Packet::import(const u_char *packet) {
	if (packet == NULL) {
		return T_ERROR;
	}

	int packetIndex = 0;

	// Copy Ethernet header
	memcpy(&ethernetHeader, &packet[0], PACKET_ETHERNET_HEADER_LENGTH);
	packetIndex += PACKET_ETHERNET_HEADER_LENGTH;

	// Copy IP Header
	memcpy(&ethernetHeader, &packet[packetIndex], PACKET_IP_HEADER_LENGTH);
	packetIndex += PACKET_IP_HEADER_LENGTH;

	// Copy UDP Header
	memcpy(&udpHeader, &packet[packetIndex], PACKET_UDP_HEADER_LENGTH);
	packetIndex += PACKET_UDP_HEADER_LENGTH;

	// Copy Data
	memcpy(&data[0], &packet[packetIndex], PACKET_DATA_LENGTH);

	return T_SUCCESS;
}

int Packet::extract(u_char *packet) {
	int packetIndex = 0;

	// Copy Ethernet header
	memcpy(&packet[0], &ethernetHeader, PACKET_ETHERNET_HEADER_LENGTH);
	packetIndex += PACKET_ETHERNET_HEADER_LENGTH;

	// Copy IP Header
	memcpy(&packet[packetIndex], &ipHeader, PACKET_IP_HEADER_LENGTH);
	packetIndex += PACKET_IP_HEADER_LENGTH;

	// Copy UDP Header
	memcpy(&packet[packetIndex], &udpHeader, PACKET_UDP_HEADER_LENGTH);
	packetIndex += PACKET_UDP_HEADER_LENGTH;

	// Copy Data
	memcpy(&packet[packetIndex], data, PACKET_DATA_LENGTH);
	packetIndex += PACKET_DATA_LENGTH;

	return T_SUCCESS;
}

void Packet::print() {
	u_char data[TOTAL_PACKET_LENGTH];
	extract(data);

	for (int i = 0; i < TOTAL_PACKET_LENGTH; i++) {
		printf("%02x", data[i]);
	}

	printf("\n");
}