#ifndef Log_h
#define Log_h

#include <iostream>
#include <string>
#include <mutex>

#define LOG_ENABLED 1

using namespace std;

class Log {
public:
	static void d(string TAG, string msg);
	static void d(string TAG, int msg);
protected:
	
};

#endif