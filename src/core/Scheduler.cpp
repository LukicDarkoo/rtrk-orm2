#include "Scheduler.h"

string Scheduler::TAG = "Scheduler";

Scheduler::Scheduler(unsigned char _type) :
type(_type), 
ackId(0),
ackWaitTime(0),
transferedPackets(0),
lastTimeMeasurements(0),
wifiTransporter(nullptr), 
ethernetTransporter(nullptr) {
	transferPath = TransferPath::TP_WIFI;

	function<void(Packet *)> callback = bind(&Scheduler::packetReceived, this, placeholders::_1);

	// Initialize wifi
	pcap_t *wifiHandler = PcapHelper::getHandler(DeviceType::WIFI);
	if (wifiHandler != nullptr) {
		wifiTransporter = new Transporter(wifiHandler);
		wifiTransporter->onPacketReceived(callback);
	}
	else {
		Log::d(TAG, "Error: there is no wifi device");
		exit(1);
	}

	// Initialize ethernet
	pcap_t *ethernetHandler = PcapHelper::getHandler(DeviceType::ETHERNET);
	if (ethernetHandler != nullptr) {
		ethernetTransporter = new Transporter(ethernetHandler);
		ethernetTransporter->onPacketReceived(callback);
	}
	else {
		Log::d(TAG, "Error: there is no wifi device");
		exit(1);
	}
	

	bufferReceiver = nullptr;
	bufferSender = nullptr;
	Log::d(TAG, "Object initialized");
}

Scheduler::~Scheduler() {
	if (wifiTransporter != nullptr) {
		delete wifiTransporter;
	}

	if (ethernetTransporter != nullptr) {
		delete ethernetTransporter;
	}

	if (bufferReceiver != nullptr) {
		delete bufferReceiver;
	}
	if (bufferSender != nullptr) {
		delete bufferSender;
	}
}

void Scheduler::changeTransferPath() {
	if (transferPath == TransferPath::TP_ETHERNET) {
		transferPath = TransferPath::TP_WIFI;
	}
	else {
		transferPath = TransferPath::TP_ETHERNET;
	}
}

void Scheduler::requestRetransmission(unsigned int packetIndex) {
	Packet packet = Packet();
	packet.setCode(CODE_REQUEST_RETRANSMISSION);
	sendPacket(packet);
}

void Scheduler::sendPacket(Packet packet) {
	switch (transferPath) {
	case TransferPath::TP_ETHERNET:
		Log::d(TAG, "Sent over ethernet");
		ethernetTransporter->sendPacket(packet);
		break;

	case TransferPath::TP_WIFI:
		Log::d(TAG, "Sent over wifi");
		wifiTransporter->sendPacket(packet);
		break;
	}
}

void Scheduler::packetReceived(Packet *packet) {
	switch (packet->getCode()) {
	case CODE_TRANSFER:
		if (type == SCHEDULER_TYPE_RECEIVER) {
			if (bufferReceiver != nullptr) {
				bufferReceiver->add(packet);

				// Send ACK
				if (bufferReceiver->isBlockFull() == true) {
					bufferReceiver->clearBuffer();
					Log::d(TAG, "Clear receiver buffer");

					Packet packet;
					packet.setCode(CODE_ACK);
					packet.setDataByte(ackId++, 0);
					sendPacket(packet);
					Log::d(TAG, "ACK sent");
				}
			}
			else {
				Log::d(TAG, "Initialize receiver!");
			}
		} 
		// ELSE: Ignore packets, because this instance of scheduler was sent them
		break;

	case CODE_FINISH:
		if (type == SCHEDULER_TYPE_RECEIVER) {
			bufferReceiver->finish();
			cout << "File transfer finished successfully. I hope. God please!" << endl;
			cout << "Transfer time: " << time(0) - startTransferTime << " seconds" << endl;
		}
		break;

	case CODE_REQUEST_RETRANSMISSION:
		// TODO: Pull to BufferSender
		break;

	case CODE_ACK:
		if (type == SCHEDULER_TYPE_SENDER) {
			Log::d(TAG, "ACK received");
			if (ackId == packet->getDataByte(0)) {
				// Increase ACK ID
				ackId++;

				// Rest ACK wait passed time
				ackWaitTime = 0;

				if (bufferSender != nullptr) {
					bufferSender->loadNextWindow();
				}
				else {
					Log::d(TAG, "Initialize sender!");
				}
			}
		}
		break;
	}
}

void Scheduler::receiveFile(string filename) {
	cout << "File transfer started to `" << filename << "`" << endl;

	bufferReceiver = new BufferReceiver(filename);
}

int Scheduler::sendFile(string filename) {
	cout << "File transfer started from `" << filename << "`" << endl;
	startTransferTime = time(0);

	bufferSender = new BufferSender(filename);

	while (bufferSender->isDone() == false) {
		if (bufferSender->isWindowOver() == false) {
			Log::d(TAG, "packet send start");
			Packet packet = bufferSender->getPacket();
			if (packet.getCode() != CODE_INVALID) {
				sendPacket(packet);
			}
			transferedPackets++;
		}
		else {
			// Wait for ACK
			this_thread::sleep_for(chrono::milliseconds(10));
			ackWaitTime += 10;

			// Start retransmission on timeout
			if (ackWaitTime > ACK_TIMEOUT) {
				bufferSender->prepareWindowRetransmission();
				ackWaitTime = 0;
				changeTransferPath();
				transferedPackets -= BUFFER_SIZE;
			}
		}


		// Bonus
		if (lastTimeMeasurements + 10 < time(0)) {
			lastTimeMeasurements = time(0);

			int speed = (transferedPackets > 0) ? PACKET_DATA_LENGTH * transferedPackets * 6 : 0;
			transferedPackets = 0;

			cout << "Average packet size: " << TOTAL_PACKET_LENGTH << " Bytes" << endl;
			cout << "Average speed: " << speed << " Bytes per minute" << endl;
			cout << "-------------------------------------------------------" << endl;
		}
	}

	return T_SUCCESS;
}
