#ifndef Headers_h
#define Headers_h

#include <stdio.h>
#include <iostream>
#include <string.h>

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#else
#include <stdlib.h>
#include <netinet/in.h>
#include <time.h>
#endif

#include "pcap.h"
#include "Config.h"
#include "Log.h"

#define PACKET_ETHERNET_HEADER_LENGTH 14
#define PACKET_IP_HEADER_LENGTH 20
#define PACKET_UDP_HEADER_LENGTH (8 + 1 + 4) // Extended for 5 byte to implement secured data transfer
#define PACKET_DATA_LENGTH (TOTAL_PACKET_LENGTH - PACKET_ETHERNET_HEADER_LENGTH - PACKET_IP_HEADER_LENGTH - PACKET_UDP_HEADER_LENGTH)

// Reference: https://technet.microsoft.com/en-us/library/cc959827.aspx
#define PROTOCOL_UDP 0x11

#define CODE_TRANSFER 0x01
#define CODE_REQUEST_RETRANSMISSION 0x02
#define CODE_ACK 0x03
#define CODE_FINISH 0x04
#define CODE_INVALID 0x05

using namespace std;

typedef struct MACAddress {
	unsigned char Byte1;
	unsigned char Byte2;
	unsigned char Byte3;
	unsigned char Byte4;
	unsigned char Byte5;
	unsigned char Byte6;
} MACAddress;

typedef struct EthernetHeader {
	MACAddress DestinationMAC;
	MACAddress SourceMAC;
	unsigned short Type;
} EthernetHeader;

// IPv4 Address
typedef struct IPAddress {
	unsigned char Byte1;
	unsigned char Byte2;
	unsigned char Byte3;
	unsigned char Byte4;
} IPAddress;

// IPv4 Header
typedef struct IPHeader {
	unsigned char VersionInternetHeaderLength;
	unsigned char TOS;
	unsigned short TotalLength;
	unsigned short Identification;
	unsigned short FlagsFragmetOffeset;
	unsigned char TimeToLive;
	unsigned char Protocol;
	unsigned short HeaderChecksum;
	IPAddress SourceAddress;
	IPAddress DestinationAddress;
} IPHeader;

// UDP Header + Extended to make it secure (Code & PacketIndex)
typedef struct UDPHeader {
	unsigned short SourcePort;
	unsigned short DestinationPort;
	unsigned short DatagramLength;
	unsigned short Checksum;
	unsigned int PacketIndex;
	unsigned char Code;

} UDPHeader;


class Packet {
public:
	static const string TAG;
	Packet();
	Packet(const u_char* packet) { import(packet); };

	// Debug, print whole packet as hexadecimal array
	void print();

	// Export `Packet` to raw data (u_char array)
	int extract(u_char* packet);

	// Convert raw data (u_char array) to `Packet`
	int import(const u_char* packet);

	// Set data
	int setData(char data[PACKET_DATA_LENGTH]);

	// We are going to use Option and Padding field from IP Header to store packet order,
	// but there is also idea to put it in packet content
	// Set Packet Index
	void setIndex(unsigned int value) { udpHeader.PacketIndex = value; }

	// Get Packet Index
	unsigned int getIndex() { return udpHeader.PacketIndex; }

	unsigned char getCode() { return udpHeader.Code; }

	void setCode(unsigned char value) { udpHeader.Code = value; };

	void setDataByte(unsigned char value, unsigned short index) { data[index] = value; }

	unsigned char getDataByte(unsigned short index) { return data[index]; }

	// Get data
	int getData(char *data);

	// Get Port
	unsigned short getPort();

	// Get protocol
	unsigned char getProtocol() { return ipHeader.Protocol; }

	// Get Ethernet Type
	unsigned short getEthernetType() { return ethernetHeader.Type; }

	// Get IP Total Length
	unsigned short getIPTotalLength() { return ipHeader.TotalLength; }
private:
	EthernetHeader ethernetHeader;
	IPHeader ipHeader;
	UDPHeader udpHeader;

	unsigned char data[PACKET_DATA_LENGTH];
};

#endif
