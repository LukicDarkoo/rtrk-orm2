#ifndef Config_h
#define Config_h

// Tunable configurations
#define TOTAL_PACKET_LENGTH 1500
#define BUFFER_SIZE 2
#define ACK_TIMEOUT 5000

// Application constants
#define PORT_NUMBER 1117
#define LOG_ENABLED 1
#define RECIEVE_SPEED 1 // Min: 1, Max: 100

#define T_SUCCESS 0
#define T_ERROR 1

#endif