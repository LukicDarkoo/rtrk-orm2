#include "PcapHelper.h"

const string PcapHelper::TAG = "PcapHelper";

const vector<string> PcapHelper::wifiDevices = vector<string>({
	"wlan0",
	"\\Device\\NPF_{35252DDF-F9E4-4EBF-AE69-F94A15ACF502}" // Loopback
	"\\Device\\NPF_{F3A07214-7B29-4663-A430-03CC25F83129}"
});
const vector<string> PcapHelper::ethernetDevices = vector<string>({ 
	"eth0",
	"\\Device\\NPF_{35252DDF-F9E4-4EBF-AE69-F94A15ACF502}" // Loopback
	"\\Device\\NPF_{2A3C7C64-ACF5-41FD-9148-32F1ED4AB3F5}"
});

pcap_t * PcapHelper::getHandler(DeviceType type) {
	pcap_if_t *alldevs;
	pcap_if_t *d;
	char errbuf[PCAP_ERRBUF_SIZE];
	int i = 0;

	// Print available devices
	if (pcap_findalldevs(&alldevs, errbuf) == -1) {
		Log::d(TAG, "No devices");
		return nullptr;
	}
	for (d = alldevs; d; d = d->next) {
		printf("%d. %s", ++i, d->name);
		if (d->description)
			printf(" (%s)\n", d->description);
		else
			printf(" (No description available)\n");
	}
	
	// Try to find proper device
	pcap_t *handle;
	vector<string> devices;
	if (type == DeviceType::WIFI) {
		devices = PcapHelper::wifiDevices;
	}
	else {
		devices = PcapHelper::ethernetDevices;
	}
	for (int i = 0; i < devices.size(); i++) {
		Log::d(TAG, devices[i]);
		handle = pcap_open_live(devices[i].c_str(), BUFSIZ, 1, 1000, errbuf);
		if (handle != NULL) {
			return handle;
		}
	}

	// Print error if it is there
	if (strlen(errbuf) > 0) {
		Log::d(TAG, errbuf);
	}

	return nullptr;
}
