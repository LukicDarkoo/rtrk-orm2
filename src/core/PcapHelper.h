#ifndef PcapHelper_h
#define PcapHelper_h

#include <string>
#include <iostream>
#include <vector>
#include <string.h>
#include "pcap.h"
#include "Config.h"
#include "Log.h"

using namespace std;

enum DeviceType { WIFI, ETHERNET };

class PcapHelper {
public:
	static const string TAG;
	static pcap_t * getHandler(DeviceType type);

private:
	static const vector<string> wifiDevices;
	static const vector<string> ethernetDevices;
};

#endif
