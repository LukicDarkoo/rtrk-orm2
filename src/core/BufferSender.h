#ifndef BufferSender_h
#define BufferSender_h

#include <string>
#include <fstream>
#include <map>
#include <mutex>
#include <thread>
#include "Config.h"
#include "Log.h"
#include "Packet.h"

using namespace std;

class BufferSender {
public:
	static string TAG;
	BufferSender(string filename);
	~BufferSender();
	bool isFull() { return BUFFER_SIZE <= buffer.size(); }
	bool isDone() { return done; }
	void loadNextWindow();
	bool isWindowOver();
	Packet getPacket();
	Packet getPacket(unsigned int packetIndex);
	void prepareWindowRetransmission();

private:
	ifstream file;
	unsigned int nextPacketIndex;
	unsigned int topPacketIndex;
	int windowIndex;

	// Idea of int key is to make fast & simple search
	map<int, Packet> buffer;

	// Disable default constructor
	BufferSender();

	void fillBuffer();
	bool done;
	bool readFileFinished;
	mutex m;
	unsigned int windowStartTopPacketIndex;
};

#endif