#ifndef Scheduler_h
#define Scheduler_h

#include <string>
#include <fstream>
#include <thread>
#include <vector>
#include "Packet.h"
#include "Transporter.h"
#include "PcapHelper.h"
#include "BufferReceiver.h"
#include "BufferSender.h"
#include "Log.h"

#define SCHEDULER_TYPE_RECEIVER 1
#define SCHEDULER_TYPE_SENDER 2

enum TransferPath { TP_WIFI, TP_ETHERNET };

using namespace std;

class Scheduler {
public:
	static string TAG;
	Scheduler(const Scheduler &scheduler) {};
	Scheduler(unsigned char type);
	~Scheduler();
	int sendFile(string filename);
	void receiveFile(string filename);
	

private:
	Scheduler();

	BufferReceiver *bufferReceiver;
	BufferSender *bufferSender;

	void packetReceived(Packet *packet);
	void changeTransferPath();
	void requestRetransmission(unsigned int packetIndex);
	void sendPacket(Packet packet);

	Transporter *wifiTransporter;
	Transporter *ethernetTransporter;

	TransferPath transferPath;

	mutex mutexFileWriter;
	unsigned char type;
	unsigned char ackId;
	unsigned int ackWaitTime;
	unsigned int lastTimeMeasurements;
	time_t startTransferTime;
	int transferedPackets;
};

#endif