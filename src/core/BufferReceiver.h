#ifndef BufferReceiver_h
#define BufferReceiver_h

#include <map>
#include <fstream>
#include <string>
#include <mutex>
#include "Packet.h"
#include "Log.h"

using namespace std;

class BufferReceiver {
public:
	static string TAG;
	BufferReceiver(string filename);
	~BufferReceiver();
	void add(Packet *packet);
	bool isBlockFull();
	void clearBuffer();
	void finish();
	
private:

	// Idea of int key is to make fast & simple search
	map<int, Packet*> buffer;
	unsigned int packetIndexInsideWindow;

	// Disable default constructor
	BufferReceiver();

	ofstream *file;
	unsigned int nextPacketIndex;
	void tryToWrite();

	string filename;
	mutex m;
};

#endif