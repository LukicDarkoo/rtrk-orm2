#ifndef Transporter_h
#define Transporter_h

#include <thread>
#include <mutex>
#include <functional>
#include "pcap.h"
#include "Config.h"
#include "Packet.h"
#include "Log.h"

using namespace std;

class Transporter {
public:
	static string TAG;
	Transporter(pcap_t *adhandle);
	~Transporter();
	void onPacketReceived(function<void(Packet *)> _onPacketReceivedCallback) { onPacketReceivedCallback = _onPacketReceivedCallback; };
	int sendPacket(Packet packet);
private:
	pcap_t *adhandle;
	function<void(Packet *)> onPacketReceivedCallback;

	void packetListenerHandler();
	thread threadPacketListener;
	mutex mutextPacketListener;
};

#endif
