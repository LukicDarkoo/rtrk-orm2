#include <iostream>
#include "Config.h"
#include "PcapHelper.h"
#include "Scheduler.h"

using namespace std;

int main() {
	Scheduler scheduler = Scheduler(SCHEDULER_TYPE_RECEIVER);

	scheduler.receiveFile("transfer_r.jpg");

	return 0;
}
